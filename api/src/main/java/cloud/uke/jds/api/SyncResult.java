package cloud.uke.jds.api;

/** @author du */
/**
 * <p>SyncResult class.</p>
 *
 * @author du
 * @version $Id: $Id
 */
public enum SyncResult {
  /** 重发该条数据 */
  RETRY,
  /** 下一条数据 */
  NEXT,
}

package cloud.uke.jds.api.internal;

import cloud.uke.jds.api.DataSyncBuilder;

/**
 * InternalStoreDataHandler interface.
 *
 * @author du
 * @version $Id: $Id
 */
public interface InternalStoreDataHandler<T, ID> {

  /**
   * setFlags.
   *
   * @param src a {@link java.lang.String} object
   * @param target a {@link java.lang.String} object
   */
  void setFlags(String src, String target);

  /**
   * 准备工作
   *
   * <p>比如初始化数据库连接，建库建表
   *
   * <p>同步完成后才会进行其他操作
   */
  default void prepare() {}

  /**
   * 保存数据
   *
   * @param t 数据
   */
  void save(T t);

  /**
   * 查询数据
   *
   * @return 从存储中拉取一条数据
   */
  InternalWrapSyncData<T, ID> poll();

  /**
   * 处理结果成功后的处理，比如修改同步状态记录
   *
   * @param data 包装的数据
   */
  void success(InternalWrapSyncData<T, ID> data);

  /** 清除所有同步的数据和状态 */
  default void veryDangerOperationCleanAllData() {}

  /** 重置同步状态，从头开始 */
  default void reset() {}

  /** 停止任务，清理资源 */
  default void stop() {}

  void setDataSyncBuilder(DataSyncBuilder<T, ID> syncBuilder);
}

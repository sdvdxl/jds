package cloud.uke.jds.api;

/**
 * 任务运行状态
 *
 * @author du
 * @version $Id: $Id
 */
public enum DataSyncStat {
  /** 初始化状态，需要调用start */
  INIT,
  /** 运行中 */
  RUNNING,
  /** 暂停中，可以再次start */
  PAUSED,
  /** 已停止，停止后不能再使用 */
  STOPPED
}

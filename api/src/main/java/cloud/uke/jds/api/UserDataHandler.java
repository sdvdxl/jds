package cloud.uke.jds.api;

/**
 * 同步数据的业务逻辑接口 @author du
 *
 * @author du
 * @version $Id: $Id
 */
public interface UserDataHandler<T, ID> {

  /**
   * 同步数据动作，需要自行实现逻辑
   *
   * <p>不建议抛出异常，用户捕获异常自行处理。 如果不慎有异常，则打印异常，根据等待时间再次执行。
   *
   * @param data 要同步的数据， 肯定有值
   * @param id 最后同步的id
   * @param src 数据来源
   * @param target 同步目标
   * @return RETRY 代表重试，会在等待时间结束后重试， NEXT 代表处理下一个
   */
  SyncResult doSync(ID id, String src, String target, T data);
}

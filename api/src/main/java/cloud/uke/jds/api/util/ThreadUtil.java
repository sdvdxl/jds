package cloud.uke.jds.api.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * ThreadUtil class.
 *
 * @author du
 * @version $Id: $Id
 */
public class ThreadUtil {

  /** @author du */
  public static class ThreadFactoryBuilder implements ThreadFactory {
    private final AtomicInteger counter = new AtomicInteger(0);
    private final String namePrefix;

    public ThreadFactoryBuilder(String namePrefix) {
      this.namePrefix = namePrefix;
    }

    public ThreadFactory build() {
      return new ThreadFactoryBuilder(namePrefix);
    }

    @Override
    public Thread newThread(Runnable r) {
      Thread thread = new Thread(r);
      thread.setName(namePrefix + "-" + counter.getAndIncrement());
      return thread;
    }
  }
}

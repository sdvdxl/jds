package cloud.uke.jds.api.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * InternalWrapSyncData class.
 *
 * @author du
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class InternalWrapSyncData<T, ID> {
  protected T data;

  protected ID lastSyncId;

  protected String src;
  protected String target;
}

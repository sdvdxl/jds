package cloud.uke.jds.api.internal;

import cloud.uke.jds.api.DataSyncBuilder;

/**
 * Abstract InternalAbstractStoreDataHandler class.
 *
 * @author du
 */
public abstract class InternalAbstractStoreDataHandler<T, ID>
    implements InternalStoreDataHandler<T, ID> {
  protected String src;
  protected String target;
  protected DataSyncBuilder<T, ID> syncBuilder;

  /** {@inheritDoc} */
  @Override
  public void setFlags(String src, String target) {
    this.src = src;
    this.target = target;
  }

  @Override
  public void setDataSyncBuilder(DataSyncBuilder<T, ID> syncBuilder) {
    this.syncBuilder = syncBuilder;
  }
}

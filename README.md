# JAVA 简单数据同步(传输)框架

## 背景

A服务（称为上游服务）推送了数据过来后，需要服务转发数据给B，C，D（称为下游服务）。上游服务发送成功后就不关心这个数据了，因此需要本服务保证下游服务都能够收到这些数据。

但是下游服务中的任一个任何时刻都有可能出现问题，比如网络不通，服务停机维护等。所以需要本服务在提供转发数据的同时，尽可能减少其他服务的影响（B停机不影响C和D的转发），所以需要并行分发，同时提供重试机制。

## 原理

1. 通过 `putData` 方法同步接收数据并持久化，保证接收数据这个地方是可靠的，如果这个地方出现问题，需要上游服务提供重试机制
2. 每个 DataSync 实例内部都有一个线程进行数据处理，start 之后，此线程就不断查询同步状态记录，将最后同步的一条记录的id作为查询条件，查询新的一条数据并返回给内部处理
3. 内部调用用户数据处理接口，用户需要实现 `UserDataHandler` 接口
4. 如果 `UserDataHandler` 成功，应该返回 `NEXT` 代表处理下一条数据；否则应该返回 `RETRY`，不建议返回异常，异常信息最好用户自己处理完成后根据业务选择返回结果
5. 根据用户返回结果处理，如果是 `NEXT` 保存最后一条记录状态，继续处理下一条数据；如果非NEXT，sleep 3s 重试

## 底层数据存储支持：

- [x] mongodb
- [ ] mysql
- [ ] redis
- [ ] elasticsearch
- [ ] kafka
- [ ] rabbitmq
- [ ] zookeeper
- [ ] ... 需要自行实现api接口

## 使用方式

添加依赖

```xml
<dependency>
  <groupId>cloud.uke.jds</groupId>
  <artifactId>mongodb-provider</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
```

尝鲜版需要加入snapshot仓库

```xml
<repositories>
    <repository>
      <id>sonatype-snapshot</id>
      <url>https://s01.oss.sonatype.org/content/repositories/snapshots/</url>
    </repository>
</repositories>
```

```java
DataSyncBuilder<Student, ObjectId> builder = DataSync.builder();
builder.setInternalStoreDataHandler(new MongoInternalStoreDataHandlerImpl<>(mongoTemplate));
builder.setUserDataHandler(
    student -> {
      System.out.println("同步数据：" + student);
      return SyncResult.NEXT;
    });
builder.setSrcId(src);
builder.setTargetId(target);
DataSync<Student, ObjectId> dataSync = builder.build();
dataSync.start();
```

参见测试用例 `DataSyncMongoProviderTest`。

## 方法介绍

- start 启动数据同步
- pause 暂停数据同步
- stop 停止数据同步(此实例无法再次使用，需要重新实例化)
- veryDangerOperationCleanAllData 删除 src 相关的原始数据，并删除src和target相关的同步状态，注意这个是非常危险的操作，数据不可恢复，所以弄了个这么长的名字以此引起你的注意
- putData 同步记录新增的数据进行持久化

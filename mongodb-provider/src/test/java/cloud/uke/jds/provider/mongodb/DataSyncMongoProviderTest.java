package cloud.uke.jds.provider.mongodb;

import cloud.uke.jds.api.DataSync;
import cloud.uke.jds.api.DataSyncBuilder;
import cloud.uke.jds.api.SyncResult;
import cloud.uke.jds.api.UserDataHandler;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import java.util.concurrent.TimeUnit;
import org.bson.types.ObjectId;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.data.mongodb.core.MongoTemplate;

public class DataSyncMongoProviderTest {
  private static MongoTemplate mongoTemplate;

  @BeforeClass
  public static void setUp() {
    MongoClient mongoClient = MongoClients.create(System.getProperty("mongo.uri"));
    mongoTemplate = new MongoTemplate(mongoClient, "Iot-Cloud-Console");
  }

  @Test
  public void testAllInOne() throws InterruptedException {
    // 如果没有mongo，可以使用docker 快速安装一个
    //    docker run -d --name mongodb -e MONGO_INITDB_ROOT_USERNAME=root -e
    // MONGO_INITDB_ROOT_PASSWORD=Iot123456 -p 27017:27017 mongodb
    // 创建一个mongo client
    MongoClient mongoClient =
        MongoClients.create(
            "mongodb://root:123456@localhost:27017/?authSource=admin&readPreference=primary&directConnection=true&ssl=false");
    // 创建 mongoTemplate
    MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, "jds");
    DataSyncBuilder<String, ObjectId> builder = DataSync.builder();
    // 设置内部存储provider
    builder.setInternalStoreDataHandler(new MongoStoreDataHandlerImpl<>(mongoTemplate));
    // 设置用户数据处理逻辑，用户实现
    builder.setUserDataHandler(
        new UserDataHandler<String, ObjectId>() {
          @Override
          public SyncResult doSync(ObjectId objectId, String src, String target, String data) {
            System.out.println("同步数据：" + data);
            return SyncResult.NEXT;
          }
        });
    // 设置来源标识，存储和拉取用这个作为数据源区分
    builder.setSrcId("src_a");
    // 设置同步目标标识符，同步进度使用这个作为区分
    builder.setTargetId("http_to_b");
    // 构造同步客户端
    DataSync<String, ObjectId> dataSync = builder.build();
    // 启动同步任务
    dataSync.start();
    // 添加待同步数据
    dataSync.putData("hello");
    // 暂停同步任务
    dataSync.pause();
    // 任务暂停，下面放数据也不会被同步
    dataSync.putData("world");
    TimeUnit.SECONDS.sleep(3);
    // 重新开始同步任务
    dataSync.start();
    // 停止任务，停止后dataSync不能再次使用，如果需要重新启动，需要重新构建 builder.build();
    dataSync.stop();
  }

  @Test
  public void testMulti() throws InterruptedException {
    DataSync<Student, ObjectId> dataSyncA = getDataSync("student", "A");
    DataSync<Student, ObjectId> dataSyncB = getDataSync("student", "B");
    dataSyncA.start();
    dataSyncB.start();

    // 发送数据使用一个client即可
    for (int i = 0; i < 3; i++) {
      dataSyncA.putData(new Student("A" + i));
    }

    TimeUnit.SECONDS.sleep(3);
    dataSyncA.pause();
    dataSyncA.reset();
    TimeUnit.SECONDS.sleep(3);

    dataSyncA.veryDangerOperationCleanAllData();
    dataSyncB.veryDangerOperationCleanAllData();
  }

  @Test
  public void test() throws InterruptedException {

    DataSync<Student, ObjectId> dataSync = getDataSync("student", "lf");
    // 启动
    dataSync.start();
    // 放数据
    dataSync.putData(new Student("sdvdxl"));
    TimeUnit.SECONDS.sleep(10);

    // 暂停
    dataSync.pause();
    dataSync.putData(new Student("paused"));
    TimeUnit.SECONDS.sleep(10);

    // 重置同步状态，从头开始
    dataSync.start();
    dataSync.pause();
    dataSync.reset();
    dataSync.start();
    TimeUnit.SECONDS.sleep(10);
  }

  private DataSync<Student, ObjectId> getDataSync(String src, String target) {
    DataSyncBuilder<Student, ObjectId> builder = DataSync.builder();
    builder.setInternalStoreDataHandler(new MongoStoreDataHandlerImpl<>(mongoTemplate));
    // 这里使用了java8 lambda表达式，不熟悉的可以直接使用匿名内部类
    builder.setUserDataHandler(
        (id, s, t, student) -> {
          System.out.println("同步数据：" + student);
          return SyncResult.NEXT;
        });
    builder.setSrcId(src);
    builder.setTargetId(target);
    DataSync<Student, ObjectId> dataSync = builder.build();
    return dataSync;
  }
}

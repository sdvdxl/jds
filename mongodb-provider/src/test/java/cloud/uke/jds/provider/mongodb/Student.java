package cloud.uke.jds.provider.mongodb;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class Student {
  @Id private ObjectId id;
  private String name;

  public Student(String name) {
    this.name = name;
  }
}

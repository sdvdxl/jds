package cloud.uke.jds.provider.mongodb;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 同步成功标记
 *
 * @author du
 * @version $Id: $Id
 */
@Document("jds_sync_store_stat")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MongoSyncStatFlag {
  private String src;
  private String target;
  /** 最后成功的一条记录 */
  private ObjectId lastId;

  private Date updatedDate;
}

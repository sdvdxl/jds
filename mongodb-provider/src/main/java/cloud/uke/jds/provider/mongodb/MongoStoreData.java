package cloud.uke.jds.provider.mongodb;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** @author du */
/**
 * <p>MongoStoreData class.</p>
 *
 * @author du
 * @version $Id: $Id
 */
@Data
@ToString(callSuper = true)
@Document("jds_sync_store_data")
@NoArgsConstructor
public class MongoStoreData<T> {

  @Id private ObjectId id;
  private T data;
  private String src;

  /**
   * <p>Constructor for MongoStoreData.</p>
   *
   * @param t a T object
   */
  public MongoStoreData(T t) {
    this.data = t;
  }
}
